var fire = require('./fire')
var db = fire.firestore()

db.settings({
    timestampsInSnapshots: true
})
var doc = db.collection('karyawan')

// var allData = []
//     db.collection('karyawan')
//     .orderBy('waktu', 'desc').get()
//     .then(snapshot => {
//         snapshot.forEach((hasil)=>{
//             allData.push(hasil.data())
//         })
//         console.log(allData)
//     }).catch((error)=>{
//         console.log(error)
//     })


const observer = db.collection('karyawan')
  .onSnapshot(querySnapshot => {
    querySnapshot.docChanges().forEach(change => {
      if (change.type === 'added') {
        console.log('New Karyawan: ', change.doc.data());
      }
      if (change.type === 'modified') {
        console.log('Modified Karyawan: ', change.doc.data());
      }
      if (change.type === 'removed') {
        console.log('Removed Karyawan: ', change.doc.data());
      }
    });
  });


// const observer = doc.onSnapshot(docSnapshot => {
//     console.log(`Received doc snapshot: ${docSnapshot}`);
//     // docSnapshot.forEach((docSingle) => {
//     //     console.log(docSingle)
//     // })
//     console.log(docSnapshot)
//     // ...
//   }, err => {
//     console.log(`Encountered error: ${err}`);
//   });