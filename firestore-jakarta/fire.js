var firebase = require('firebase')

var config = {
    apiKey: "AIzaSyB8OOKlEyCF1VnujHgEK9oNGFZDA9ZzLoY",
    authDomain: "firestore-softdev.firebaseapp.com",
    databaseURL: "https://firestore-softdev-default-rtdb.firebaseio.com",
    projectId: "firestore-softdev",
    storageBucket: "firestore-softdev.appspot.com",
    messagingSenderId: "442382510890",
    appId: "1:442382510890:web:6dbe323d0c3433340e0c53",
    measurementId: "G-N6M8XY4H2K"
};

var fire = firebase.initializeApp(config);
module.exports = fire